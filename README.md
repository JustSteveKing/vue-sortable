# Vue 2 Sortable implementation

<p align="center">
    <a href="https://vuejs.org" target="_blank" rel="noopener noreferrer">
        <img width="100" src="https://vuejs.org/images/logo.png" alt="Vue logo">
    </a>
</p>

This Vue2 implementation is a component based wrapper around the Shopify Draggable library.

## How to use

First we need to import and initialize

``` es6
import Vue from 'vue'
import Sortable from 'vue2-sortable'

Vue.use(Sortable)
```

Then in our Vue templates:

``` html
<template>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>A Sortable List</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <sortable-list v-model="items" stop-drag="drag:stop" @drag:stop="stopped">
                    <ul class="list-group" slot-scope="{ items, itemClass, handleClass }">
                        <sortable-item v-for="item in items" :key="item.id">
                            <li class="list-group-item">
                                <h5>{{ item.name }}</h5>
                                <p>{{ item.description }}</p>
                                <sortable-handle>
                                    <span>drag me</span>
                                </sortable-handle>
                            </li>
                        </sortable-item>
                    </ul>
                </sortable-list>
            </div>
        </div>
    </div>
</template>
<script>
    export default {
        data () {
            return {
                items: [
                    {
                        id: 1,
                        name: 'Test 1',
                        description: '123'
                    },
                    {
                        id: 2,
                        name: 'Test 2',
                        description: 'abc'
                    },
                    {
                        id: 3,
                        name: 'Test 3',
                        description: 'xyz'
                    },
                    {
                        id: 4,
                        name: 'Test 4',
                        description: 'qwerty'
                    },
                    {
                        id: 5,
                        name: 'Test 5',
                        description: 'jsdnf',
                    }
                ]
            }
        },

        methods: {
            stopped () {
                console.log('Dragging stopped')
            }
        }
    }
</script>
```

### Props

The only component that requires props is the `<sortable-list>` component, they are outlined below:

``` es6
    value: {
        required: true,
    },
    itemClass: {
        required: false,
        type: String,
        default: 'sortable-item',
    },
    handleClass: {
        required: false,
        type: String,
        default: 'sortable-handle'
    },
    stopDrag: {
        required: false,
        type: String
    }
```

- `value` : An array of items to be looped over
- `itemClass` : An override class for each item which can be dragged
- `handleClass` : An override class for the handle of which we want to bind the dragging to
- `stopDrag` : A callback function to be ran when dragging has stopped

### Links

[Shopify Draggable](https://github.com/Shopify/draggable)

[Vue JS](https://vuejs.org/)
