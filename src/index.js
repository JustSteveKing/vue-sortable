import { SortableList, SortableItem, SortableHandle } from './components'

export default {
    install (Vue, options) {
        Vue.component('sortable-list', SortableList)
        Vue.component('sortable-item', SortableItem)
        Vue.component('sortable-handle', SortableHandle)
    }
}
